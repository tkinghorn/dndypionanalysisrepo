#!/bin/bash

#This calls the RunCreatedNdySystematic.C file in the macros directory which loads the necessary
#libraries and then runs createdNdySystematic.cxx in the src/analysis directory.

######################################################
#SET THE CORRECTED SPECTRA FILE WITH EXTRACTED DNDY NAME AND PATH (FULL PATH)
correctedSpectraFile=../userfiles/AlAu_4_9GeV_2015/analysis/tpcPionSpectraFullResultsDBE.root

#SET THE SYSTEMATIC CORRECTED SPECTRA FILE WITH EXTRACTED DNDY NAME AND PATH (FULL PATH)
systematicCorrectedSpectraFile=../userfiles/AlAu_4_9GeV_2015/analysis/tpcPionSpectraFullResultsDMB.root

#SET THE OUTPUT DIRECTORY HERE
outputDirectory=../userfiles/AlAu_4_9GeV_2015/analysis/

#SET THE OUTPUT FILE NAME
outputFile=tpcPiondNdyComparison.root


#SET THE PARTICLE ID (PION = 0)
pid=0
#SET THE CHARGE OF THE PARTICLE
charge=-1
#SET THE ENERGY
energy=4.9
#SET THE EVENT CONFIGURATION
eventConfig=FixedTarget2015
#SET THE STARLIBRARY VERSION
starlib=SL16a

######################################################

outFile=$outputDirectory/$outputFile
root -l -b -q ../macros/RunCreatedNdySystematic.C\(\"$correctedSpectraFile\",\"$systematicCorrectedSpectraFile\",\"$outFile\",$pid,$charge,\"$starlib\",$energy,\"$eventConfig\"\) #> /dev/null 2>&1 &
exit
