#!/bin/bash

#This calls the DrawAllRapiditySpectraForAllCentrality.C file in the drawingmacros directory which loads the necessary
#libraries.

######################################################
#SET THE CORRECTED SPECTRA FILE WITH EXTRACTED DNDY NAME AND PATH (FULL PATH)
correctedSpectraFile=../userfiles/AlAu_4_9GeV_2015/analysis/tpcPionSpectraFullResultsDBE.root

#SET THE PARTICLE ID (PION = 0)
pid=0
#SET THE CHARGE OF THE PARTICLE
charge=-1
#SET THE ENERGY
energy=4.9
#SET THE EVENT CONFIGURATION
eventConfig=FixedTarget2015
#SET THE SYSTEM
system=Al\+Au

#SET THE CENTRALITY INDEX (USE -1 FOR ALL)
centralityIndex=-1
#SET THE NUCLEON-NUCLEON CENTER OF MASS RAPIDITY
rapidityValue=-1.62
#SET THE STARTING MT-M0 VALUE (GeV)
mTm0StartValue=0
#HAS THE SPECTRA BEEN CORRECTED? (IF YES, TRUE. IF NO, FALSE)
corrected=true

######################################################

root -l -b -q ../drawingmacros/DrawAllRapiditySpectraForAllCentrality.C\(\"$correctedSpectraFile\",\"$eventConfig\",\"$system\",$energy,$pid,$charge,$centralityIndex,$rapidityValue,$mTm0StartValue,$corrected\) #> /dev/null 2>&1 &

exit
