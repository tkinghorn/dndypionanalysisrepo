#!/bin/bash

#This calls the RunFitSpectraAnddNdy.C file in the macros directory which loads the necessary
#libraries and then runs fitSpectraPion.cxx in the src/analysis directory.

######################################################
#SET THE CORRECTED SPECTRA FILE NAME AND PATH (FULL PATH)
correctedSpectraFile=../userfiles/AlAu_4_9GeV_2015/analysis/tpcPionSpectraCentAll.root

#SET THE OUTPUT DIRECTORY HERE
outputDirectory=../userfiles/AlAu_4_9GeV_2015/analysis/

#SET THE OUTPUT FILE NAME
outputFile=tpcPionSpectraFullResults.root


#SET THE PARTICLE ID (PION = 0)
pid=0
#SET THE CHARGE OF THE PARTICLE
charge=-1
#SET THE ENERGY
energy=4.9
#SET THE SPECTRA FITTING FUNCTION (0 FOR DOUBLE BOSE-EINSTEIN FITTING FUNCTION, 1 FOR DOUBLE EXPONENTIAL FITTING FUNCTION, -1 FOR BOTH)
spectraFit=-1
#SET THE EVENT CONFIGURATION
eventConfig=FixedTarget2015
#SET THE STARLIBRARY VERSION
starlib=SL16a

#SET THE CENTRALITY BIN INDEX (USE -999 FOR ALL)
centralityIndex=-999
#SET THE RAPIDITY BIN INDEX (USE -999 FOR ALL)
rapidityIndex=-999

######################################################

processID=()
outFiles=()

if [[ $spectraFit -eq -1 ]]; then

  for i in {0..1}; do

    outFile=$(basename $outputFile .root)
    if [[ $i -eq 0 ]]; then
      outFile=$outputDirectory/"$outFile"DBE.root
      spectraFit=0
    elif [[ $i -eq 1 ]]; then
      outFile=$outputDirectory/"$outFile"DMB.root
      spectraFit=1
    fi
      root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$correctedSpectraFile\",\"$outFile\",$pid,$charge,$energy,\"$eventConfig\",\"$starlib\",$i,$centralityIndex,$rapidityIndex\) > /dev/null 2>&1 &

    outFiles+=($outFile)
    processID+=($!)
    echo ${processID[@]}
  done
  wait ${processsID[@]}
  exit
elif [[ $spectraFit -eq 0 ]] || [[ $spectraFit -eq 1 ]]; then
  outFile=$(basename $outputFile .root)
  if [[ $spectraFit -eq 0 ]]; then
    outFile=$outputDirectory/"$outFile"DBE.root
  elif [[ $spectraFit -eq 1 ]]; then
    outFile=$outputDirectory/"$outFile"DMB.root
  fi
  root -l -b -q ../macros/RunFitSpectraAnddNdy.C\(\"$correctedSpectraFile\",\"$outFile\",$pid,$charge,$energy,\"$eventConfig\",\"$starlib\",$spectraFit,$centralityIndex,$rapidityIndex\) > /dev/null 2>&1 &
  outFiles+=($outFile)
  processID+=($!)
  echo ${processID[@]}
  wait ${processID[@]}
else
  echo "spectraFit parameter not set to one of: -1,0,1. Please choose one of those values."
fi

exit
