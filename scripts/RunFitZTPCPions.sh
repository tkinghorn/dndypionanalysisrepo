#!/bin/bash

#This calss the RunFitZTPCPions.C file in the macros directory which loads the necessary
#libraries and then runs fitZTPCPions.cxx in the src/analysis directory.

######################################################
#SET THE YIELD HISTOGRAM FILE NAME AND PATH (FULL PATH)
yieldHistoFile=../userfiles/AlAu_4_9GeV_2015/analysis/YieldHistograms.root

#SET THE OUTPUT DIRECTORY HERE
outputDirectory=../userfiles/AlAu_4_9GeV_2015/analysis/

#SET THE SPECTRA FILE NAME
spectraFile=tpcPionSpectraCent.root

#SET THE OUTPUT IMAGE DIRECTORY (LEAVE BLANK TO SAVE NO IMAGES)
imageDir=../userfiles/AlAu_4_9GeV_2015/analysis/Images/

#SET THE RAPIDITY VALUE (USE -999 FOR ALL)
#rapidityValue=-1.62
rapidityValue=-999
#SET THE CENTRALITY BIN INDEX (USE -1 FOR ALL)
centralityIndex=-1

#SET THE STARLIBRARY VERSION
starlib=SL16a
######################################################

processID=()
outFiles=()

if [[ $centralityIndex -eq -1 ]]; then

  for i in {0..5}; do

    outFile=$(basename $spectraFile .root)
    outFile=$outputDirectory/"$outFile"$i\_temp.root

    outFiles+=($outFile)

    root -l -b -q ../macros/RunFitZTPCPions.C\(\"$yieldHistoFile\",\"$outFile\",\"$starlib\",$i,$rapidityValue,\"$imageDir\"\) > /dev/null 2>&1 &

    processID+=($!)
    echo ${processID[@]}
  done

  wait ${processsID[@]}
  hadd $outputDirectory/"$(basename $spectraFile .root)"All.root ${outFiles[@]}
  wait
  rm ${outFiles[@]}
else
  outFile=$(basename $spectraFile .root)
  outFile=$outputDirectory/"$outFile"$centralityIndex\.root
  root -l -b -q ../macros/RunFitZTPCPions.C\(\"$yieldHistoFile\",\"$outFile\",\"$starlib\",$centralityIndex,$rapidityValue,\"$imageDir\"\) #> /dev/null 2>&1 &
fi

exit
