#!/bin/bash

#This runs the RunCorrectSpectra.C  macro which loads the necessary libraries
#and then runs the code that calls a correction chain and applies it to the raw spectra.

###########################################################
#SET THE UNCORRECTED SPECTRA FILE NAME HERE
#spectraFileName=../userfiles/AlAu_4_9GeV_2015/analysis/tpcPionSpectraFull_sysErrorCheck.root
spectraFileName=../userfiles/AlAu_4_9GeV_2015/analysis/tpcPionSpectraCentAll.root

#SET THE CORRECTION CURVES FILE NAME HERE
correctionFileName=../userfiles/AlAu_4_9GeV_2015/embedding/pimCurves.root

#SET THE PID 
pid=0

#SET THE CHARGE
charge=-1

#SET THE STAR LIBRARY
starLibrary=SL16a

#SET THE ENERGY
energy=4.9

#SET THE EVENT CONFIGURATION HERE
eventConfig=FixedTarget2015

#SET THE CORRECTIONS
DoInterBinCorrection=false
DoMuonCorrection=false
DoHadronicCorrection=false
DoFeedDownCorrection=false
DoEnergyLossCorrection=false
DoEfficiencyCorrection=true

########################################################### 

root -l -b -q ../macros/RunCorrectSpectra.C\(\"$spectraFileName\",\"$correctionFileName\",$pid,$charge,\"$starLibrary\",$energy,\"$eventConfig\",$DoInterBinCorrection,$DoMuonCorrection,$DoHadronicCorrection,$DoFeedDownCorrection,$DoEnergyLossCorrection,$DoEfficiencyCorrection\)

exit
