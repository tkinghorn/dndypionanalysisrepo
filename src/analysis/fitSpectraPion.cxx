//Fit the Spectra

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TAxis.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraFitFunctions.h"

#include <fstream>
using namespace std;

bool draw = true;

//Fit Functions to the spectra
Double_t DoubleBoseEinsteinFitFunction(Double_t *x, Double_t *par){
	return BoseEinsteinFitFuncInRange(x,par) + BoseEinsteinFitFuncInRange(x,&par[5]);
}
Double_t DoubleBoltzmannFitFunction(Double_t *x, Double_t *par){
  return mTExponentialFitFuncInRange(x,par) + mTExponentialFitFuncInRange(x,&par[5]);
  //return BoltzmannFitFuncInRange(x,par) + BoltzmannFitFuncInRange(x,&par[5]);
}

void fitSpectraPion(TString spectraFileName, TString resultFileName, Int_t pid, Int_t charge,
    Int_t spectraFittingFunction = 0, Int_t userCentBin = -999, Double_t userRapidity = -999){

  ofstream myfile;
  myfile.open("dNdyPion.txt");
  myfile << "Cent" <<'\t'<< "y" <<'\t'<< "dN/dy" <<'\t'<< "dN/dyErr" <<endl;
  
  gStyle->SetOptFit(1);
  gStyle->SetErrorX(0.);
  ParticleInfo *particleInfo = new ParticleInfo();
  const int nCentBins = GetNCentralityBins();

  Int_t spectraFitFunction = spectraFittingFunction;

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"READ");

  //Create the Output Results File
  TFile *resultsFile = new TFile(resultFileName,"RECREATE");
  resultsFile->cd();
  resultsFile->mkdir(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("dNdyLowT_%s",particleInfo->GetParticleName(pid,charge).Data()));
  resultsFile->mkdir(Form("SpectraFits_%s",particleInfo->GetParticleName(pid,charge).Data()));    
  //Create a Canvas if drawing
  TCanvas *canvas = NULL;
  TCanvas *dNdyCanvas = NULL;
  TCanvas *dNdyComparisonCanvas = NULL;
  if (draw){
    cout<<"DRAWING !!!"<<endl;
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    dNdyCanvas = new TCanvas("dNdyCanvas","dNdyCanvas",20,750,1200,600);
    dNdyCanvas->Divide(2,1);
    dNdyCanvas->cd(1);
    gPad->DrawFrame(-2,0,2,150);
    dNdyCanvas->cd(2);
    gPad->DrawFrame(-2,0,2,0.4);
  }

  
  std::vector<TMultiGraph *> dNdyGraphMulti(nCentBins,(TMultiGraph *)NULL);
  std::vector<TGraphErrors *> dNdyGraphTotal(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> dNdyGraphLowT(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> dNdyGraphHighT(nCentBins,(TGraphErrors *)NULL);

  std::vector<TGraphErrors *> tSlopeGraph1(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> tSlopeGraph2(nCentBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> dNdyGraphLowTRound0(nCentBins,(TGraphErrors *)NULL);
  
  TMultiGraph *dNdyGraphHighTMulti = new TMultiGraph();
  TMultiGraph *dNdyGraphComparisonMulti = new TMultiGraph();

  //Local Pointers
  TGraphErrors *tpcSpectrum, *tofSpectrum;
  TF1 *fitFuncHigh, *fitFuncLow, *fitFunc1, *fitFunc2, *fitFunc, *fitFuncExtrapolation, *thermalFitFunc, *fitFuncBoltzmann;

  //Get the User's rapidity bin
  Int_t userRapidityBin = GetRapidityIndex(userRapidity);
  if (userRapidity != -999 && userRapidityBin < 0){
    cout <<"ERROR: FitSpectra - Invalid value of rapidity. EXITING!\n";
    exit (EXIT_FAILURE);
  }

  //Loop Over the Centrality and rapidity bins and fit the spectra
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    cout <<"iCentBin = "<<iCentBin <<endl;
    //If the user has specified a valid cent bin,
    //then skip all others
    if (userCentBin != -999 && userCentBin >= 0 && userCentBin < nCentBins && iCentBin != userCentBin)
      continue;

    resultsFile->cd();
    dNdyGraphMulti.at(iCentBin) = new TMultiGraph();
    //dNdyGraphMulti.at(iCentBin)->GetYaxis()->SetLimits(0.,35.);
    dNdyGraphMulti.at(iCentBin)->SetMinimum(0.);
    dNdyGraphMulti.at(iCentBin)->SetMaximum(35.);    

    dNdyGraphTotal.at(iCentBin) = new TGraphErrors();
    dNdyGraphLowT.at(iCentBin) = new TGraphErrors();
    dNdyGraphHighT.at(iCentBin) = new TGraphErrors();
    tSlopeGraph1.at(iCentBin) = new TGraphErrors();
    tSlopeGraph2.at(iCentBin) = new TGraphErrors();
    dNdyGraphLowTRound0.at(iCentBin) = new TGraphErrors();
 
    dNdyGraphTotal.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    dNdyGraphLowT.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    dNdyGraphHighT.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    tSlopeGraph1.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    tSlopeGraph2.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    dNdyGraphLowTRound0.at(iCentBin)->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
    
    dNdyGraphTotal.at(iCentBin)->SetMarkerColor(1);
    dNdyGraphLowT.at(iCentBin)->SetMarkerColor(4);
    dNdyGraphHighT.at(iCentBin)->SetMarkerColor(iCentBin+1);
    if(iCentBin==4){dNdyGraphHighT.at(iCentBin)->SetMarkerColor(46);}
    tSlopeGraph1.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    tSlopeGraph2.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));
    dNdyGraphLowTRound0.at(iCentBin)->SetMarkerColor(particleInfo->GetParticleColor(pid));


		//Fit for low dN/dy
		TF1 *dNdyFit1 = new TF1("dNdyFit1","pol1",-2,2);
		dNdyFit1->SetNpx(10000);	
    
    TGraphErrors *dNdyFit1Conf = new TGraphErrors(dNdyFit1->GetNpx());
    dNdyFit1Conf->SetFillColor(particleInfo->GetParticleColor(pid));
    dNdyFit1Conf->SetFillStyle(3001);

    Double_t dNdyStepSize = (2-(-2))/(double)dNdyFit1Conf->GetN();
    for(int i=0; i<dNdyFit1Conf->GetN(); i++){
      dNdyFit1Conf->SetPoint(i,-2 + i*dNdyStepSize,0);
    }
    //Fits and Confidence intervals of the slope parameters
    TF1 *tSlopeFit1 = new TF1("tSlopeFit1","pol 0",-2,2);// new TF1("tSlopeFit1","gaus",-2,2);
    TF1 *tSlopeFit2 = new TF1("tSlopeFit2","gaus",-2,2);
    tSlopeFit1->SetNpx(10000);
    tSlopeFit2->SetNpx(10000);
    
    TGraphErrors *tSlopeConf1 = new TGraphErrors(tSlopeFit1->GetNpx());
    TGraphErrors *tSlopeConf2 = new TGraphErrors(tSlopeFit2->GetNpx());
    tSlopeConf1->SetFillColor(particleInfo->GetParticleColor(pid));
    tSlopeConf2->SetFillColor(particleInfo->GetParticleColor(pid)+2);
    tSlopeConf1->SetFillStyle(3001);
    tSlopeConf2->SetFillStyle(3001);
    Double_t stepSize = (2-(-2))/(double)tSlopeConf1->GetN();
    for (int i=0; i<tSlopeConf1->GetN(); i++){
      tSlopeConf1->SetPoint(i,-2 + i*stepSize,0);
      tSlopeConf2->SetPoint(i,-2 + i*stepSize,0);
    }
    
    //We do two rounds of fitting. The first to parameterize the slope,
    //and the second to extract the dNdy.
 
	  Double_t mindNdyRangeFit = 0.0;
	  Double_t maxdNdyRangeFit = 10.0;

    Double_t minRangeFit = 0;
    Double_t maxRangeFit = 1;
    
    for (int iRound=0; iRound<2; iRound++){
      
      for (int yIndex=2; yIndex<nRapidityBins; yIndex++){
      	cout << "yIndex = " << yIndex << '\t'<< nRapidityBins <<endl;
	      //Skip Rapidity bins not of interest
	      if (yIndex < GetMinRapidityIndexOfInterest() || yIndex > GetMaxRapidityIndexOfInterest())
	      continue;
	
	      //If the user has specified a vaid rapidity value,
	      //then skip all other rapidity bins
	      if (userRapidityBin >= 0 && userRapidityBin != yIndex)
	        continue;
	
      	//Compute Rapidity
	      Double_t rapidity = GetRapidityRangeCenter(yIndex);
	
	      //Get the Spectra
	      spectraFile->cd();
	      spectraFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
        //cout<<"====== cout : "<<Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data())<<endl;
	      tpcSpectrum = (TGraphErrors *)gDirectory->Get(Form("correctedSpectra_%s_Cent%02d_yIndex%02d",
				      			   particleInfo->GetParticleName(pid,charge).Data(),
							         iCentBin,yIndex));
      	tofSpectrum = (TGraphErrors *)gDirectory->Get(Form("correctedSpectraTOF_%s_Cent%02d_yIndex%02d",
			      				   particleInfo->GetParticleName(pid,charge).Data(),
						      	   iCentBin,yIndex));
        
      	//Make sure the tpc Spectrum Exists
      	if (!tpcSpectrum){
      	  cout <<Form("INFO - TPC Spectrum Does Not Exist! CentIndex:%d, yIndex:%d \n",iCentBin,yIndex);
      	  continue;
      	}

      	//Make sure the tpc Spectrum has points
      	if (tpcSpectrum->GetN() < 5){
      	  cout <<Form("INFO - TPC Spectrum Does Not have enough points! CentIndex:%d, yIndex:%d \n",
		            iCentBin,yIndex)<<endl;
      	  delete tpcSpectrum;
      	  tpcSpectrum = NULL;
      	  continue;
      	}
	
      	//Make sure the tof spectrum exists and has points
      	if (tofSpectrum){
      	  if (tofSpectrum->GetN() < 4){
      	    delete tofSpectrum;
      	    tofSpectrum = NULL;
      	  }
      	}
      	else {
      	  tofSpectrum = NULL;
      	}

        //Include systematic errors on spectra points
        for(Int_t iPoint=0; iPoint<tpcSpectrum->GetN(); ++iPoint){

          Double_t sysError = .05;
          Double_t mTm0Value = tpcSpectrum->GetX()[iPoint];
          if(yIndex==2){
            if(mTm0Value>.33 && mTm0Value<.34)
              sysError = .1;
            if(mTm0Value>.31 && mTm0Value<.32)
              sysError = .18;
            if(mTm0Value>.28 && mTm0Value<.29)
              sysError = .25;
            if(mTm0Value>.26 && mTm0Value<.27)
              sysError = .4;
            if(mTm0Value>.23 && mTm0Value<.24)
              sysError = .55;
            if(mTm0Value>.21 && mTm0Value<.22)
              sysError = .8;
          }
          if(yIndex==3){
            if(mTm0Value>.16 && mTm0Value<.17)
              sysError = .1;
            if(mTm0Value>.13 && mTm0Value<.14)
              sysError = .25;
            if(mTm0Value>.11 && mTm0Value<.12)
              sysError = .4;
           }
          if(yIndex==4){
            if(mTm0Value>.16 && mTm0Value<.17)
              sysError = .08;
            if(mTm0Value>.13 && mTm0Value<.14)
              sysError = .18;
            if(mTm0Value>.11 && mTm0Value<.12)
              sysError = .3;
            if(mTm0Value>.08 && mTm0Value<.09)
              sysError = .4;
          }
          if(yIndex==5){
            if(mTm0Value>.13 && mTm0Value<.14)
              sysError = .1;
            if(mTm0Value>.11 && mTm0Value<.12)
              sysError = .18;
            if(mTm0Value>.08 && mTm0Value<.09)
              sysError = .3;
            if(mTm0Value>.06 && mTm0Value<.07)
              sysError = .5;
          }
          if(yIndex==6){
            if(mTm0Value>.11 && mTm0Value<.12)
              sysError = .1;
            if(mTm0Value>.08 && mTm0Value<.09)
              sysError = .2;
            if(mTm0Value>.06 && mTm0Value<.07)
              sysError = .3;
            if(mTm0Value>.03 && mTm0Value<.04)
              sysError = .6;
          }
          if(yIndex==7){
            if(mTm0Value>.11 && mTm0Value<.12)
              sysError = .08;
            if(mTm0Value>.08 && mTm0Value<.09)
              sysError = .12;
            if(mTm0Value>.06 && mTm0Value<.07)
              sysError = .2;
            if(mTm0Value>.03 && mTm0Value<.04)
              sysError = .4;
          }
          if(yIndex==8){
            if(mTm0Value>.11 && mTm0Value<.12)
              sysError = .06;
            if(mTm0Value>.08 && mTm0Value<.09)
              sysError = .1;
            if(mTm0Value>.06 && mTm0Value<.07)
              sysError = .15;
            if(mTm0Value>.03 && mTm0Value<.04)
              sysError = .4;
          }
          if(yIndex==9){
            if(mTm0Value>.08 && mTm0Value<.09)
              sysError = .08;
            if(mTm0Value>.06 && mTm0Value<.07)
              sysError = .1;
            if(mTm0Value>.03 && mTm0Value<.04)
              sysError = .3;
          }
          if(yIndex==10){
            if(mTm0Value>.06 && mTm0Value<.07)
              sysError = .08;
            if(mTm0Value>.03 && mTm0Value<.04)
              sysError = .2;
          }
          if(yIndex==11){
            if(mTm0Value>.03 && mTm0Value<.04)
              sysError = .1;
          }

          //cout << "cent, yIndex, yield, error:" <<'\t'<< iCentBin <<", "<< yIndex <<", "<< tpcSpectrum->GetY()[iPoint] <<", "<< tpcSpectrum->GetEY()[iPoint] <<endl;

          Double_t ey = tpcSpectrum->GetEY()[iPoint];

          tpcSpectrum->SetPointError(iPoint,
                                    tpcSpectrum->GetEX()[iPoint],
                                    pow(pow(tpcSpectrum->GetEY()[iPoint],2) 
                                      + pow(tpcSpectrum->GetY()[iPoint]*sysError,2),.5));

          cout << "cent, yIndex, yield, staterror, statsyserror:" <<'\t'<< iCentBin <<", "<< yIndex <<", "<< tpcSpectrum->GetY()[iPoint] <<", "<< ey <<", "<< tpcSpectrum->GetEY()[iPoint] <<endl;
        
        }
                        
      	//Ranges over which to compute the dNdy from the fits.
      	//Notes: 1. We must subtract/add half the value of the bin widths (x error)
      	//       to the beginning and ending point since the dNdy counting takes
      	//       into account the full bin width of all measured points.
        //Double_t minRangeFit = 0.1;
        //Double_t maxRangeFit = 0.6;
        
        if (/*iCentBin==0 && */yIndex==2) {minRangeFit=0.4; maxRangeFit=1.0;} //0.8
        if (/*iCentBin==0 && */yIndex==3) {minRangeFit=0.3; maxRangeFit=1.0;} //0.8
        if (/*iCentBin==0 && */yIndex==4) {minRangeFit=0.2; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==5) {minRangeFit=0.125; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==6) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==7) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==8) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==9) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==10) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==11) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==12) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==13) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==14) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==15) {minRangeFit=0.1; maxRangeFit=1.0;}
        if (/*iCentBin==0 && */yIndex==16) {minRangeFit=0.1; maxRangeFit=1.0;} //high .5
        if (/*iCentBin==0 && */yIndex==17) {minRangeFit=0.1; maxRangeFit=1.0;} //high .6 
        if (/*iCentBin==0 && */yIndex==18) {minRangeFit=0.1; maxRangeFit=1.0;} //high .5

        maxRangeFit = tpcSpectrum->GetX()[tpcSpectrum->GetN()-1];


        if(tpcSpectrum){
          //TGraphChop(tpcSpectrum,minRangeFit,true);
          //TGraphChop(tpcSpectrum,maxRangeFit,false);
        }

        if (pid == PION){
    			//cout <<"BEFORE fitFunc"<<endl;
          if(spectraFitFunction==0){
            fitFunc = new TF1("fitFunc",DoubleBoseEinsteinFitFunction,minRangeFit,maxRangeFit,10);
            fitFuncExtrapolation = new TF1("fitFuncExtrapolation",DoubleBoseEinsteinFitFunction,0,minRangeFit,10);
          }
          if(spectraFitFunction==1){
            fitFunc = new TF1("fitFunc",DoubleBoltzmannFitFunction,minRangeFit,maxRangeFit,10);
            fitFuncExtrapolation = new TF1("fitFuncExtrapolation",DoubleBoltzmannFitFunction,0,minRangeFit,10);
          }
          fitFunc->SetName(Form("SpectraFit_%s_Cent%02d_yIndex%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin,yIndex));
          fitFuncExtrapolation->SetName(Form("SpectraFit_%s_Cent%02d_yIndex%02d_Extrapolation",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin,yIndex));
    			fitFunc->SetParameter(0,5);
    			fitFunc->SetParameter(1,.060); //.16
		    	fitFunc->FixParameter(2,particleInfo->GetParticleMass(pid));
    			fitFunc->FixParameter(3,mindNdyRangeFit);
    			fitFunc->FixParameter(4,maxdNdyRangeFit);
		
    			fitFunc->SetParameter(5,10);
    			fitFunc->SetParameter(6,.170); //.16
    			fitFunc->FixParameter(7,particleInfo->GetParticleMass(pid));
    			fitFunc->FixParameter(8,mindNdyRangeFit);
    			fitFunc->FixParameter(9,maxdNdyRangeFit);   

			
		    	//cout <<"MIDDLE fitFunc"<<endl;
    			//cout <<"AFTER fitFunc"<<endl;

		    	fitFunc->SetParNames(
				    "#frac{dN}{dy}","T_{low}",Form("m_{%s}",particleInfo->GetParticleSymbol(PION,-1).Data()),"","",
    				"#frac{dN}{dy}","T_{high}",Form("m_{%s}",particleInfo->GetParticleSymbol(PION,-1).Data()),"","");
        }
      	//cout<<"After pid == PION, after fitFunc-> "<<endl;
      	//Fit this is the first round the set limits on the tSlope Parameters
      	if (iRound == 0){
      	  fitFunc->SetParLimits(1,.05,.075);
      	  fitFunc->SetParLimits(6,.150,.190);
          
      	}
	
      	//If this is the second round then fix the tSlope Parameters and low T dNdy
      	if (iRound == 1){
      	  fitFunc->FixParameter(1,tSlopeFit1->Eval(rapidity));
      	  fitFunc->FixParameter(6,tSlopeFit2->Eval(rapidity));
      	  //fitFunc->SetParLimits(1,.05,.07);
      	  //fitFunc->SetParLimits(6,.150,.190);
          fitFunc->FixParameter(0,dNdyFit1->Eval(rapidity));
        }
      	//cout<<"Before 'Perform the Fit' "<<endl;
      	//Perform the Fit
      	Int_t fitStatus(-1);
      	Int_t nAttempts(0);
      	while (fitStatus != 0 && nAttempts < 5){
      	  //fitStatus = tpcSpectrum->Fit(fitFuncLow,"REX0");
      	  fitStatus = tpcSpectrum->Fit(fitFunc,"REX0B");
      	  nAttempts++;
      	}
      	if (tofSpectrum){
      	  fitStatus = -1;
      	  nAttempts = 0;
      	  while (fitStatus != 0 && nAttempts < 5){
            //fitStatus = tofSpectrum->Fit(fitFuncHigh,"REX0");
    	      fitStatus = tofSpectrum->Fit(fitFunc,"REX0B");
    	      nAttempts++;
      	  }
      	}
      	else{
      	  fitStatus = -1;
      	  nAttempts = 0;
      	  while (fitStatus != 0 && nAttempts < 5){
      	    //fitStatus = tpcSpectrum->Fit(fitFuncHigh,iRound == 1 ? "R+EX0":"REX0");
      	    fitStatus = tpcSpectrum->Fit(fitFunc,iRound == 1 ? "R+EX0B":"REX0B");
      	    nAttempts++;
      	  }
      	} 
        if(spectraFitFunction==0){
          fitFuncLow = new TF1("BoseEinstein_LowT",BoseEinsteinFitFuncInRange,0,2,5);
          fitFuncHigh = new TF1("BoseEinstein_HighT",BoseEinsteinFitFuncInRange,0,2,5);
          for(Int_t i=0; i<5; ++i){
            fitFuncLow->FixParameter(i,fitFunc->GetParameter(i));
            fitFuncHigh->FixParameter(i,fitFunc->GetParameter(i+5));
          } 
          fitFuncLow->SetName(Form("BoseEinsteinLowT_%s_Cent%02d_yIndex%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin,yIndex));
          fitFuncHigh->SetName(Form("BoseEinsteinHighT_%s_Cent%02d_yIndex%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin,yIndex));
        }
        if(spectraFitFunction==1){
          fitFuncLow = new TF1("MaxwellBoltzmann_LowT",mTExponentialFitFuncInRange,0,2,5);
          fitFuncHigh = new TF1("MaxwellBoltzmann_HighT",mTExponentialFitFuncInRange,0,2,5);
          for(Int_t i=0; i<5; ++i){
            fitFuncLow->FixParameter(i,fitFunc->GetParameter(i));
            fitFuncHigh->FixParameter(i,fitFunc->GetParameter(i+5));
          } 
          fitFuncLow->SetName(Form("MaxwellBoltzmannLowT_%s_Cent%02d_yIndex%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin,yIndex));
          fitFuncHigh->SetName(Form("MaxwellBoltzmannHighT_%s_Cent%02d_yIndex%02d",
              particleInfo->GetParticleName(pid,charge).Data(),
              iCentBin,yIndex));
        }

        if (iRound == 1){
          resultsFile->cd(Form("SpectraFits_%s",particleInfo->GetParticleName(pid,charge).Data()));
          for(Int_t iParam=0; iParam<10; iParam++)
            fitFuncExtrapolation->SetParameter(iParam,fitFunc->GetParameter(iParam));
          fitFuncExtrapolation->SetRange(0,1);
          fitFunc->Write();
          fitFuncExtrapolation->Write();
          fitFuncLow->Write();
          fitFuncHigh->Write();
        }
        resultsFile->cd();
    		//cout<<"After 'Perform the Fit' "<<endl;
        if (iRound == 1){
          Double_t Chi2 = fitFunc->GetChisquare();
          Double_t ndf = fitFunc->GetNDF();
          Double_t Chi2ndf = fitFunc->GetChisquare()/fitFunc->GetNDF();
          cout << "===================================================================================="<<endl;
          cout <<"Chi2ndf for iCentBin " << iCentBin << ", yIndex " << yIndex << " = " << Chi2ndf <<endl;        
          cout << "===================================================================================="<<endl;
          cout <<"minRangeFit = " << minRangeFit <<endl;
          cout <<"maxRangeFit = " << maxRangeFit <<endl; 
        }
      	//Extract the Slope Parameters if this is the first round
      	if (iRound == 0){
      	  tSlopeGraph1.at(iCentBin)->SetPoint(tSlopeGraph1.at(iCentBin)->GetN(),
      					      GetRapidityRangeCenter(yIndex),
      					      fitFunc->GetParameter(1));
      	  tSlopeGraph1.at(iCentBin)->SetPointError(tSlopeGraph1.at(iCentBin)->GetN()-1,
                      rapidityBinWidth/2.0,
      						    fitFunc->GetParError(1));
	  
      	  tSlopeGraph2.at(iCentBin)->SetPoint(tSlopeGraph2.at(iCentBin)->GetN(),
			      		      GetRapidityRangeCenter(yIndex),
      					      fitFunc->GetParameter(6));
      	  tSlopeGraph2.at(iCentBin)->SetPointError(tSlopeGraph2.at(iCentBin)->GetN()-1,
			    			      rapidityBinWidth/2.0,
        						  fitFunc->GetParError(6));

          dNdyGraphLowTRound0.at(iCentBin)->SetPoint(dNdyGraphLowTRound0.at(iCentBin)->GetN(),
                      GetRapidityRangeCenter(yIndex),
                      fitFunc->GetParameter(0));
          dNdyGraphLowTRound0.at(iCentBin)->SetPointError(dNdyGraphLowTRound0.at(iCentBin)->GetN()-1,
                      rapidityBinWidth/2.0,
                      fitFunc->GetParError(0));
    	  }

      	//Extract the dNdy if this is the second round
      	if (iRound == 1){

    	    Double_t dNdyLow = fitFunc->GetParameter(0);
    	    Double_t dNdyHigh = fitFunc->GetParameter(5);

      		cout <<"yIndex = " <<'\t'<< yIndex <<endl;
      	  cout <<"Rapidity = " <<'\t'<< GetRapidityRangeCenter(yIndex) <<endl;
          cout <<"dNdyLow = "<<'\t'<< dNdyLow <<endl;
          cout <<"dNdyHigh = "<<'\t'<< dNdyHigh <<endl;
        	Double_t dNdy = dNdyLow + dNdyHigh;
      		cout <<"dNdy = " <<'\t'<< dNdy <<endl;

          Double_t dNdyErr = sqrt(pow(fitFunc->GetParError(5),2));
      
          myfile << iCentBin <<'\t'<< GetRapidityRangeCenter(yIndex) <<'\t'<< dNdyHigh <<'\t'<< dNdyErr <<endl;

          //**********************************
          //Set Point on dNdy Graph
          //**********************************
     
          if(yIndex>3){
       
            dNdyGraphTotal.at(iCentBin)->SetPoint(dNdyGraphTotal.at(iCentBin)->GetN(),
                     -1*GetRapidityRangeCenter(yIndex)/3.24,
                     dNdy);
            dNdyGraphTotal.at(iCentBin)->SetPointError(dNdyGraphTotal.at(iCentBin)->GetN()-1,
                    0,//rapidityBinWidth/2.0,
                    dNdyErr);
            dNdyGraphLowT.at(iCentBin)->SetPoint(dNdyGraphLowT.at(iCentBin)->GetN(),
                    -1*GetRapidityRangeCenter(yIndex)/3.24,
                    dNdyLow);
            dNdyGraphLowT.at(iCentBin)->SetPointError(dNdyGraphLowT.at(iCentBin)->GetN()-1,
                    0,//rapidityBinWidth/2.0,
                    0/*dNdyErr*/);
            dNdyGraphHighT.at(iCentBin)->SetPoint(dNdyGraphHighT.at(iCentBin)->GetN(),
                    -1*GetRapidityRangeCenter(yIndex)/3.24,
                    dNdyHigh);
            dNdyGraphHighT.at(iCentBin)->SetPointError(dNdyGraphHighT.at(iCentBin)->GetN()-1,
                    0,//rapidityBinWidth/2.0,
                    dNdyErr);
            
            dNdyGraphMulti.at(iCentBin)->Add(dNdyGraphTotal.at(iCentBin),"AP");
            dNdyGraphMulti.at(iCentBin)->Add(dNdyGraphLowT.at(iCentBin),"P");
            dNdyGraphMulti.at(iCentBin)->Add(dNdyGraphHighT.at(iCentBin),"P");

          }
        }//End of Round 1
            
        if (draw){
          canvas->cd();
          canvas->DrawFrame(0,0.01*TMath::MinElement((tofSpectrum!=NULL ? tofSpectrum->GetN():tpcSpectrum->GetN()),
                        (tofSpectrum!=NULL ? tofSpectrum->GetY():tpcSpectrum->GetY())),
                        2.0,20*TMath::MaxElement(tpcSpectrum->GetN(),tpcSpectrum->GetY()));
          canvas->SetLogy();
          tpcSpectrum->Draw("PZ");
          if (tofSpectrum)
            tofSpectrum->Draw("PZ");

          //fitFuncLow->Draw("SAME");
          //fitFuncHigh->Draw("SAME");
          fitFunc->Draw("SAME");
          canvas->Update();
          
          if (iRound == 1){
            dNdyCanvas->cd(1);
            //dNdyGraph.at(iCentBin)->Draw("PZ");
            //dNdyGraphLowT.at(iCentBin)->Draw("PZ");
            //dNdyGraphhighT.at(iCentBin)->Draw("PZ");
            dNdyGraphMulti.at(iCentBin)->Draw("AP");
            dNdyGraphMulti.at(iCentBin)->GetXaxis()->SetTitle("y");
            dNdyGraphMulti.at(iCentBin)->GetYaxis()->SetTitle("dN/dy");
            gPad->Modified();
            if(iCentBin==0){
              dNdyComparisonCanvas->cd();
              dNdyGraphHighT.at(iCentBin)->Draw("AP");
            }
    
          }
          if (iRound == 0){
            dNdyCanvas->cd(2);
            tSlopeGraph1.at(iCentBin)->Draw("PZ");
            tSlopeGraph2.at(iCentBin)->Draw("PZ");
            dNdyGraphLowTRound0.at(iCentBin)->Draw("PZ");
          }
          
          dNdyCanvas->Update();
          if (iRound == 1)
            gSystem->Sleep(2000);
        }

        if (iRound == 1){
          //Save Spectra
          resultsFile->cd();
          resultsFile->cd(Form("CorrectedSpectra_%s",particleInfo->GetParticleName(pid,charge).Data()));
          if (tpcSpectrum)
            tpcSpectrum->Write();
          if (tofSpectrum)
            tofSpectrum->Write();
        }
   
      }//End Loop Over yIndex
      


      //cout <<"Why am I fitting here!" <<endl;
      
      //Fit the Slope Parameter Graphs
      if (iRound == 0){
        	
        tSlopeFit1->SetParameter(0,.55);
        tSlopeFit2->SetParameter(0,.170);
        if (iCentBin==0) {/*tSlopeFit1->SetParameter(1,-1.146);*/ tSlopeFit2->SetParameter(1,-1.146);}
        if (iCentBin==1) {/*tSlopeFit1->SetParameter(1,-1.203);*/ tSlopeFit2->SetParameter(1,-1.203);}
        if (iCentBin==2) {/*tSlopeFit1->SetParameter(1,-1.249);*/ tSlopeFit2->SetParameter(1,-1.249);}
        if (iCentBin==3) {/*tSlopeFit1->SetParameter(1,-1.295);*/ tSlopeFit2->SetParameter(1,-1.295);}
        if (iCentBin==4) {/*tSlopeFit1->SetParameter(1,-1.334);*/ tSlopeFit2->SetParameter(1,-1.334);}
        if (iCentBin==5) {/*tSlopeFit1->SetParameter(1,-1.336);*/ tSlopeFit2->SetParameter(1,-1.336);}
        ///tSlopeFit1->SetParLimits(2,1.0,2.0);
        tSlopeFit1->SetParLimits(0,.053,.057);
        tSlopeFit2->SetParLimits(2,1.55,1.75);
        cout <<"Before the tSlopeFit"<<endl;
        //cout << "tSlopeFit1 = " <<  tSlopeFit1->GetParameter(1) <<" ; tSlopeFit2 = " <<'\t'<< tSlopeFit2->GetParameter(1) <<endl;

      	tSlopeGraph1.at(iCentBin)->Fit(tSlopeFit1,"BEX0","",-2,-0.5);
      	(TVirtualFitter::GetFitter())->GetConfidenceIntervals(tSlopeConf1,0.68);	
      	tSlopeGraph2.at(iCentBin)->Fit(tSlopeFit2,"BEX0","",-2,-0.5);
      	(TVirtualFitter::GetFitter())->GetConfidenceIntervals(tSlopeConf2,0.68);	
        cout <<"After the tSlopeFit:"<<endl;
        //cout << "tSlopeFit1 = " <<  tSlopeFit1->GetParameter(1) <<" ; tSlopeFit2 = " <<'\t'<< tSlopeFit2->GetParameter(1) <<endl;
        dNdyGraphLowTRound0.at(iCentBin)->Fit(dNdyFit1,"EX0","",-1.3,-0.5);
        (TVirtualFitter::GetFitter())->GetConfidenceIntervals(dNdyFit1,0.68);

      	if (draw){
      	  dNdyCanvas->cd(2);
      	  tSlopeConf1->Draw("2");
      	  tSlopeConf2->Draw("2");
      	  dNdyFit1Conf->Draw("2");
      	  dNdyCanvas->Update();
      	}
	
      }
      
      
    }//End Loop Over fitting Rounds

    //Set Names
    dNdyGraphMulti.at(iCentBin)->SetName(Form("RapidityDensity_MultiGraph_%s_Cent%02d",
					 particleInfo->GetParticleName(pid,charge).Data(),
					 iCentBin));
    dNdyGraphMulti.at(iCentBin)->SetTitle(Form("#pi^{-} RapidityDensity Cent %d-%d%;y;#frac{dN}{dy}",
            iCentBin*5,
            (iCentBin+1)*5));

    dNdyGraphTotal.at(iCentBin)->SetName(Form("RapidityDensity_Total_%s_Cent%02d",
					 particleInfo->GetParticleName(pid,charge).Data(),
					 iCentBin));
    dNdyGraphTotal.at(iCentBin)->GetXaxis()->SetTitle("y");
    dNdyGraphTotal.at(iCentBin)->GetYaxis()->SetTitle("#frac{dN}{dy}");
    
		dNdyGraphLowT.at(iCentBin)->SetName(Form("RapidityDensity_LowT_%s_Cent%02d",
					 particleInfo->GetParticleName(pid,charge).Data(),
					 iCentBin));
		dNdyGraphLowT.at(iCentBin)->GetXaxis()->SetTitle("y");
    dNdyGraphLowT.at(iCentBin)->GetYaxis()->SetTitle("#frac{dN}{dy}");

    dNdyGraphHighT.at(iCentBin)->SetName(Form("RapidityDensity_HighT_%s_Cent%02d",
					 particleInfo->GetParticleName(pid,charge).Data(),
					 iCentBin));
		dNdyGraphHighT.at(iCentBin)->GetXaxis()->SetTitle("y");
    dNdyGraphHighT.at(iCentBin)->GetYaxis()->SetTitle("#frac{dN}{dy}");

    tSlopeGraph1.at(iCentBin)->SetName(Form("SlopeParameterLowmTm0_%s_Cent%02d",
					    particleInfo->GetParticleName(pid,charge).Data(),
					    iCentBin));
    tSlopeGraph2.at(iCentBin)->SetName(Form("SlopeParameterHighmTm0_%s_Cent%02d",
					    particleInfo->GetParticleName(pid,charge).Data(),
					    iCentBin));
    tSlopeFit1->SetName(Form("SlopeParameterLowmTm0Fit_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    tSlopeFit2->SetName(Form("SlopeParameterHighmTm0Fit_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    tSlopeConf1->SetName(Form("SlopeParameterLowmTm0Conf_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    tSlopeConf2->SetName(Form("SlopeParameterHighmTm0Conf_%s_Cent%02d",
			      particleInfo->GetParticleName(pid,charge).Data(),
			      iCentBin));
    dNdyGraphLowTRound0.at(iCentBin)->SetName(Form("dNdyLowTRound0_%s_Cent%02d",
					    particleInfo->GetParticleName(pid,charge).Data(),
					    iCentBin));
    dNdyFit1->SetName(Form("dNdyLowTFit_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));
    dNdyFit1Conf->SetName(Form("dNdyLowTConf_%s_Cent%02d",
            particleInfo->GetParticleName(pid,charge).Data(),
            iCentBin));

    dNdyGraphHighTMulti->Add(dNdyGraphHighT.at(iCentBin),"AP");

    //Save
    resultsFile->cd();
    resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraphTotal.at(iCentBin)->GetN() > 0){
      dNdyGraphMulti.at(iCentBin)->Write();
      dNdyGraphTotal.at(iCentBin)->Write();
      dNdyGraphLowT.at(iCentBin)->Write();
      dNdyGraphHighT.at(iCentBin)->Write();
		}
    resultsFile->cd(Form("TSlopeParameter_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (tSlopeGraph1.at(iCentBin)->GetN() > 0){
      tSlopeGraph1.at(iCentBin)->Write();
      tSlopeFit1->Write();
      tSlopeConf1->Write();
    }
    if (tSlopeGraph2.at(iCentBin)->GetN() > 0){
      tSlopeGraph2.at(iCentBin)->Write();
      tSlopeFit2->Write();
      tSlopeConf2->Write();
    }    
    resultsFile->cd(Form("dNdyLowT_%s",particleInfo->GetParticleName(pid,charge).Data()));
    if (dNdyGraphLowTRound0.at(iCentBin)->GetN() > 0){
      dNdyGraphLowTRound0.at(iCentBin)->Write();
      dNdyFit1->Write();
      dNdyFit1Conf->Write();
    }

    
  }//End Loop Over centBins


  resultsFile->cd();
  cout <<"made it here"<<endl; 
  resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  dNdyGraphHighTMulti->SetTitle(Form("#pi^{-} RapidityDensity;y_{lab}/y_{beam};#frac{dN}{dy}"));
  dNdyGraphHighTMulti->SetName("dNdyGraphHighTMulti");
  dNdyGraphHighTMulti->SetMinimum(0.);
  dNdyGraphHighTMulti->SetMaximum(22.);
  cout <<"made it here2"<<endl;
  dNdyGraphHighTMulti->Write();
  
  cout <<"made it here3"<<endl;
  resultsFile->cd();
  cout <<"made it here4"<<endl;
  resultsFile->cd(Form("SpectraFits_%s",particleInfo->GetParticleName(pid,charge).Data()));
  cout <<"made it here5"<<endl;
    
  resultsFile->cd();
  resultsFile->cd(Form("RapidityDensity_%s",particleInfo->GetParticleName(pid,charge).Data()));
  myfile.close(); 
}
