#include <iostream>
#include <vector>

#include <TFile.h>
#include <TMath.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TVirtualFitter.h>
#include <TEfficiency.h>
#include <TMinuit.h>
#include <TRandom3.h>
#include <TFitResult.h>
#include <TAxis.h>
#include <TMarker.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "ParticleInfo.h"
#include "UserCuts.h"
#include "SpectraFitFunctions.h"
#include "EfficiencyFitUtilities.h"

#include <fstream>
using namespace std;

bool draw = false;

void createdNdySystematic(TString dNdy1, TString dNdy2, TString comparisonFile, Int_t pid, Int_t charge){

  ofstream myfile;
  myfile.open("dNdyPionMinus.txt");
  myfile << "cent" <<'\t'<< "y" <<'\t'<< "dNdy" <<'\t' << "dNdyStatErr" <<'\t'<< "dNdySysErr" <<endl;

  gStyle->SetOptFit(1); 
  
  //Get the Number of Centrality Bins
  int nCentBins = GetNCentralityBins();
  std::vector<double> centralityPercents = GetCentralityPercents();
  ParticleInfo *particleInfo = new ParticleInfo();
  
  //Read the input correction files to be compared
  TFile *fdNdy1 = new TFile(dNdy1,"READ");
  TFile *fdNdy2 = new TFile(dNdy2,"READ");

  //Create the output comparison file
  TFile *sysFile = new TFile(comparisonFile,"RECREATE");
  sysFile->cd();  

	if(sysFile->GetDirectory("RapidityDensity_PionMinus") == 0 && sysFile->GetDirectory("Differences") == 0){
		sysFile->mkdir("RapidityDensity_PionMinus");
		sysFile->mkdir("Differences");
	}

  Double_t yIZ[] = {.354,.371,.389,.400,.412,.422};
  Int_t n = 15; //number of points in READ dNdy root files
  Double_t x1[n], x2[n], ex1[n], ex2[n];
  Double_t y1[n], y2[n], ey1[n], ey2[n];
  Double_t difference[n];     

  TGraphErrors *dNdyGraph1 = new TGraphErrors(11);
  TGraphErrors *dNdyGraph2 = new TGraphErrors(11);
  TGraphErrors **sysGraph = new TGraphErrors*[6];
  TGraphErrors **sysGraphCopy = new TGraphErrors*[6];
  TGraphErrors **sysGraphCopyNoFit = new TGraphErrors*[6];
  TGraphErrors **diffGraph = new TGraphErrors*[6];
  for (Int_t iCentBin=0; iCentBin<6; iCentBin++){
    sysGraph[iCentBin] = NULL;
    sysGraphCopy[iCentBin] = NULL;
    diffGraph[iCentBin] = NULL;
  }
  
  TMultiGraph *dNdyGraphHighTMulti = new TMultiGraph();
	TMultiGraph *dNdyGraphComparisonMulti = new TMultiGraph();

  dNdyGraphHighTMulti->SetName(Form("dNdyGraphHighTMulti"));
  dNdyGraphHighTMulti->SetTitle(Form(";y_{lab}/y_{beam};#frac{dN}{dy}_{high}"));
  dNdyGraphComparisonMulti->SetName(Form("dNdyGraphComparisonMulti"));
  dNdyGraphComparisonMulti->SetTitle(Form(";y_{lab}/y_{beam};#frac{dN}{dy}"));
 
  TGraphErrors *E802 = new TGraphErrors(11);
  TGraphErrors *E810 = new TGraphErrors(10);
  
	Double_t yBeamE802 = 3.5;
  E802->SetPoint(0,0.70/yBeamE802,14.3); E802->SetPointError(0,0,0.9);
  E802->SetPoint(1,0.90/yBeamE802,16.1); E802->SetPointError(1,0,0.8);
  E802->SetPoint(2,1.10/yBeamE802,16.9); E802->SetPointError(2,0,0.5);
  E802->SetPoint(3,1.30/yBeamE802,20.2); E802->SetPointError(3,0,0.5);
  E802->SetPoint(4,1.50/yBeamE802,17.5); E802->SetPointError(4,0,0.5);
  E802->SetPoint(5,1.70/yBeamE802,15.4); E802->SetPointError(5,0,0.5);
  E802->SetPoint(6,1.90/yBeamE802,12.1); E802->SetPointError(6,0,0.4);
  E802->SetPoint(7,2.10/yBeamE802,12.0); E802->SetPointError(7,0,0.2);
  E802->SetPoint(8,2.30/yBeamE802,9.96); E802->SetPointError(8,0,0.11);
  E802->SetPoint(9,2.50/yBeamE802,7.58); E802->SetPointError(9,0,0.10);
  E802->SetPoint(10,2.70/yBeamE802,5.72); E802->SetPointError(10,0,0.09);

  E802->SetMarkerColor(8);
  E802->SetMarkerSize(3);
  E802->SetMarkerStyle(kFullSquare);
	

  Double_t yBeamE810 = 3.4;
  E810->SetPoint(0,1.8/yBeamE810,18.5); E810->SetPointError(0,0,0.5);
  E810->SetPoint(1,2.0/yBeamE810,16.0); E810->SetPointError(1,0,0.2);
  E810->SetPoint(2,2.2/yBeamE810,12.6); E810->SetPointError(2,0,0.2);
  E810->SetPoint(3,2.4/yBeamE810,9.8); E810->SetPointError(3,0,0.2);
  E810->SetPoint(4,2.6/yBeamE810,7.1); E810->SetPointError(4,0,0.2);
  E810->SetPoint(5,2.8/yBeamE810,5.3); E810->SetPointError(5,0,0.2);
  E810->SetPoint(6,3.0/yBeamE810,3.9); E810->SetPointError(6,0,0.2);
  E810->SetPoint(7,3.2/yBeamE810,2.8); E810->SetPointError(7,0,0.2);
  E810->SetPoint(8,3.4/yBeamE810,1.7); E810->SetPointError(8,0,0.2);
  E810->SetPoint(9,3.6/yBeamE810,1.0); E810->SetPointError(9,0,0.2);

  E810->SetMarkerStyle(22);
  E810->SetMarkerColor(6);
  E810->SetMarkerSize(4);

  E802->Write("E802");
  E810->Write("E810");

  dNdyGraphComparisonMulti->Add(E802,"AP");
  dNdyGraphComparisonMulti->Add(E810,"AP");

  TMarker *marker0 = new TMarker(0,0,kFullStar);
  marker0->SetMarkerColor(1);
  marker0->SetMarkerSize(4);
  TMarker *marker1 = new TMarker(0,0,kFullStar);
  marker1->SetMarkerColor(2);
  marker1->SetMarkerSize(4);
  TMarker *marker2 = new TMarker(0,0,kFullStar);
  marker2->SetMarkerColor(3);
  marker2->SetMarkerSize(4);
  TMarker *marker3 = new TMarker(0,0,kFullStar);
  marker3->SetMarkerColor(4);
  marker3->SetMarkerSize(4);
  TMarker *marker4 = new TMarker(0,0,kFullStar);
  marker4->SetMarkerColor(46);
  marker4->SetMarkerSize(4);
  TMarker *marker5 = new TMarker(0,0,kFullStar);
  marker5->SetMarkerColor(6);
  marker5->SetMarkerSize(4);
  
  //Single point
  TGraphErrors* onept= new TGraphErrors(1);
  onept->SetPoint(0,1,0);

  TLegend *leg = new TLegend(.65,.14,.85,.34);
  leg->SetLineColor(kWhite);
  leg->SetBorderSize(0);
  leg->SetFillColor(kWhite);
  leg->SetTextSize(.035);
  leg->AddEntry(marker0,"0-5\% Centrality","P");
  leg->AddEntry(marker1,"5-10\% Centrality","P");
  leg->AddEntry(marker2,"10-15\% Centrality","P");
  leg->AddEntry(marker3,"15-20\% Centrality","P");
  leg->AddEntry(marker4,"20-25\% Centrality","P");
  leg->AddEntry(marker5,"25-30\% Centrality","P");


	dNdyGraphHighTMulti->SetName(Form("dNdyGraphHighTMulti"));
  dNdyGraphHighTMulti->SetTitle(Form(";y_{lab}/y_{beam};#frac{dN}{dy}_{high}"));	

  TF1* f0 = new TF1("f0","gaus(0)",0,1);
  TF1* f1 = new TF1("f1","gaus(0)",0,1);
  TF1* f2 = new TF1("f2","gaus(0)",0,1);
  TF1* f3 = new TF1("f3","gaus(0)",0,1);
  TF1* f4 = new TF1("f4","gaus(0)",0,1);
  TF1* f5 = new TF1("f5","gaus(0)",0,1);

  TF1 **gaus = new TF1*[6];
  for (Int_t iCentBin=0; iCentBin<6; iCentBin++){
    char fname[6];
    sprintf(fname,"PionGaussian_Cent%d",iCentBin);
    gaus[iCentBin] = new TF1("fname","gaus(0)",0,1);
    gaus[iCentBin]->SetLineColor(4);
    gaus[iCentBin]->SetParNames("Amp","#mu","#sigma");
  }

  TF1 *widthPionFit = new TF1("widthPionFit","pol0",.5,5.5);
  TGraphErrors *widthPion = new TGraphErrors(6);

  TString plot = "HighT";

  //Loop over Centrality Bins
  for(Int_t iRound=0; iRound<2; ++iRound){
    for (Int_t iCentBin=0; iCentBin<nCentBins; ++iCentBin){
      if(iRound==0){
        dNdyGraph1 = (TGraphErrors *)fdNdy1->Get(Form("RapidityDensity_%s/RapidityDensity_%s_%s_Cent%02d",
                                        particleInfo->GetParticleName(pid,charge).Data(),
                                        plot.Data(),
                                        particleInfo->GetParticleName(pid,charge).Data(),
                                        iCentBin));
        dNdyGraph2 = (TGraphErrors *)fdNdy2->Get(Form("RapidityDensity_%s/RapidityDensity_%s_%s_Cent%02d",
                                        particleInfo->GetParticleName(pid,charge).Data(),
                                        plot.Data(),
                                        particleInfo->GetParticleName(pid,charge).Data(),
                                        iCentBin));

        if(!dNdyGraph1 || !dNdyGraph2)
          continue;

        Double_t exzero[n], eyzero[n];
        Double_t newerror[n];


        //cout << "yLab/yBeam" <<'\t'<< "dNdy" <<'\t'<< "dNdyStatErr" <<'\t'<< "dNdySysErr" <<endl;
        for(Int_t i=0; i<n; ++i){
          x1[i] = dNdyGraph1->GetX()[i];// + .03*(Double_t)iCentBin/(Double_t)nCentBins;
          ex1[i] = dNdyGraph1->GetEX()[i];
          x2[i] = dNdyGraph2->GetX()[i];// + .03*(Double_t)iCentBin/(Double_t)nCentBins;
          ex2[i] = dNdyGraph2->GetEX()[i];
          y1[i] = dNdyGraph1->GetY()[i];
          ey1[i] = dNdyGraph1->GetEY()[i];
          y2[i] = dNdyGraph2->GetY()[i];
          ey2[i] = dNdyGraph2->GetEY()[i];
          if(iCentBin!=-3){
            difference[i] = TMath::Abs(y1[i]-y2[i]);
          }
          else{
            difference[i] = ey1[i];
          }
          newerror[i] = pow(pow(difference[i],2) + pow(ey1[i],2),.5);
          exzero[i]=0;//0.003;
          eyzero[i]=0;
          //if(x1[i] != x2[i])
            //continue;

          if (true){myfile<< iCentBin <<'\t'<< x1[i] <<'\t'<<  y1[i] <<'\t'<< ey1[i] <<'\t'<< difference[i] <<endl;}
          //if (true){myFile << iCentBin <<'\t'<< -1*yBeam*x1[i] <<'\t'<<  y1[i] <<'\t'<< ey1[i] <<'\t'<< difference[i] <<endl;}
          //if (true){myfile << iCentBin <<'\t'<< -1*yBeam*x1[i] <<'\t'<<  y1[i] <<'\t'<< newerror[i] <<endl;}

        }

        sysFile->cd("RapidityDensity_PionMinus");
        sysGraph[iCentBin] = new TGraphErrors(n,x1,y1,ex1,ey1);
        sysGraphCopy[iCentBin]= new TGraphErrors(n,x1,y1,exzero,difference);
        sysGraphCopyNoFit[iCentBin]= new TGraphErrors(n,x1,y1,exzero,difference);
        sysGraph[iCentBin]->SetMarkerSize(4);
        sysGraphCopy[iCentBin]->SetMarkerSize(4);

        gaus[iCentBin]->SetParameters(TMath::MaxElement(sysGraphCopy[iCentBin]->GetN(),sysGraphCopy[iCentBin]->GetY()),yIZ[iCentBin],.2);

        cout << iCentBin <<'\t'<< gaus[iCentBin]->GetParameter(0) <<'\t'<< gaus[iCentBin]->GetParameter(1) <<'\t'<< gaus[iCentBin]->GetParameter(2) <<endl;          

        sysGraphCopy[iCentBin]->Fit(gaus[iCentBin],"REM");
        widthPion->SetPoint(iCentBin,iCentBin+.5,gaus[iCentBin]->GetParameter(2)); widthPion->SetPointError(iCentBin,0,gaus[iCentBin]->GetParError(2));
      
        //Varying the amplitude
        //f0->FixParameter(0,f0->GetParameter(0)-.20*f0->GetParameter(0));
      
        //Varying the width
        //f0->FixParameter(2,f0->GetParameter(2)+.02);
        }
        if(iRound==1){
          gaus[iCentBin]->FixParameter(0,gaus[iCentBin]->GetParameter(0));
          gaus[iCentBin]->FixParameter(2,widthPionFit->GetParameter(0));
          sysGraphCopy[iCentBin]->Fit(gaus[iCentBin],"REM");
          cout << iCentBin <<'\t'<< gaus[iCentBin]->GetParameter(0) <<'\t'<< gaus[iCentBin]->GetParameter(1) <<'\t'<< gaus[iCentBin]->GetParameter(2) <<endl;          

          sysGraph[iCentBin]->SetName(Form("RapidityDensity_%s_%s_Cent%02d",
                                 plot.Data(),
                                 particleInfo->GetParticleName(pid,charge).Data(),
                                 iCentBin));
          sysGraph[iCentBin]->SetTitle(Form("%s; y_{lab}/y_{beam};#frac{dN}{dy}",
                    dNdyGraph1->GetTitle(),
                    particleInfo->GetParticleSymbol(pid,charge).Data()));
          sysGraph[iCentBin]->GetXaxis()->SetLabelSize(2);
          sysGraph[iCentBin]->GetYaxis()->SetLabelSize(2);
          sysGraphCopy[iCentBin]->SetName(Form("RapidityDensity_%s_%s_Cent%02d_Copy",
                                 plot.Data(),
                                 particleInfo->GetParticleName(pid,charge).Data(),
                                 iCentBin));
          sysGraphCopy[iCentBin]->SetTitle(Form(";y_{lab}/y_{beam};#frac{dN}{dy}"));//,
                    //dNdyGraph1->GetTitle(),
                    //particleInfo->GetParticleSymbol(pid,charge).Data()));
          sysGraph[iCentBin]->SetMarkerStyle(29);//particleInfo->GetParticleMarker(pid,charge));
          sysGraphCopy[iCentBin]->SetMarkerStyle(29);//particleInfo->GetParticleMarker(pid,charge));
          sysGraph[iCentBin]->SetMarkerColor(iCentBin+1);
          sysGraphCopy[iCentBin]->SetMarkerColor(iCentBin+1);
          sysGraphCopy[iCentBin]->SetFillStyle(3005);
          if(iCentBin==4){sysGraph[iCentBin]->SetMarkerColor(46); sysGraphCopy[iCentBin]->SetMarkerColor(46);}
          sysGraph[iCentBin]->Write();
          sysGraphCopy[iCentBin]->Write();
          
          dNdyGraphHighTMulti->Add(sysGraph[iCentBin],"AP");
          dNdyGraphHighTMulti->Add(sysGraphCopy[iCentBin],"AP5");
          //dNdyGraphHighTMulti->Add(E810,"AP");
          //dNdyGraphHighTMulti->Add(E802,"AP");
          
          if(iCentBin==0){
            dNdyGraphComparisonMulti->Add(sysGraph[iCentBin],"AP");
            dNdyGraphComparisonMulti->Add(sysGraphCopyNoFit[iCentBin],"AP5");
          }

          sysFile->cd("Differences");
          diffGraph[iCentBin] = new TGraphErrors(n,x1,difference);
          diffGraph[iCentBin]->SetName(Form("RapidityDensity_%s_%s_Cent%02d",
                                 particleInfo->GetParticleName(pid,charge).Data(),
                                 plot.Data(),
                                 particleInfo->GetParticleName(pid,charge).Data(),
                                 iCentBin));
          diffGraph[iCentBin]->SetTitle(Form("%s; y_{lab}/y_{beam};#frac{dN}{dy}",
                    dNdyGraph1->GetTitle(),
                    particleInfo->GetParticleSymbol(pid,charge).Data()));
          diffGraph[iCentBin]->SetMarkerStyle(particleInfo->GetParticleMarker(pid,charge));
          diffGraph[iCentBin]->SetMarkerColor(particleInfo->GetParticleColor(pid));
          diffGraph[iCentBin]->Write();
          sysFile->cd("RapidityDensity_PionMinus");
        }
      }
    widthPion->Fit(widthPionFit,"REM");
  }
  widthPion->Write("widthPion");

  TPaveText *starPrelim  = new TPaveText(.25,.14,.60,.20,"BRNBNDC");
  starPrelim->SetFillColor(kWhite);
  starPrelim->SetBorderSize(0);
  starPrelim->SetTextFont(63);
  starPrelim->SetTextSize(18);
  starPrelim->AddText("STAR PRELIMINARY");

  TPaveText *title = new TPaveText(.15,.75,.20,.85,"NBNDCBR");
  title->SetFillColor(kWhite);
  title->SetBorderSize(0);
  title->SetTextSize(.04);
  title->SetTextAlign(12);
  title->AddText(Form("#pi^{-} Rapidity Density"));
  title->AddText(Form("Al+Au #sqrt{s_{NN}} = 4.9 GeV"));

  TPaveText *title2 = new TPaveText(.55,.75,.60,.85,"NBNDCBR");
  title2->SetFillColor(kWhite);
  title2->SetBorderSize(0);
  title2->SetTextSize(.04);
  title2->SetTextAlign(12);
  title2->AddText(Form("#pi^{-} Rapidity Density"));
  
  TLegend *leg2 = new TLegend(.15,.04,.35,.34);
  leg2->SetLineColor(kWhite);
  leg2->SetBorderSize(0);
  leg2->SetFillColor(kWhite);
  leg2->SetTextSize(.030);
  leg2->AddEntry(marker0,"#splitline{STAR Al+Au FXT}{#sqrt{s_{NN}} = 4.9 GeV 0-5\% Centrality}","P");
  if(plot=="HighT") leg2->AddEntry(E802,"#splitline{AGS E-802 Si+Au}{#sqrt{s_{NN}} = 5.5 GeV 0-7\% Centrality}","P");
  if(plot=="HighT") leg2->AddEntry(E810,"#splitline{AGS E-810 Si+Pb}{#sqrt{s_{NN}} = 5.4 GeV 0-9\% Centrality}","P");
  TLegend *leg3 = new TLegend(.15,.14,.35,.34);
  leg3->SetLineColor(kWhite);
  leg3->SetBorderSize(0);
  leg3->SetFillColor(kWhite);
  leg3->SetTextSize(.030);
  leg3->AddEntry(E810,"#splitline{AGS E-810 Si+Pb}{#sqrt{s_{NN}} = 5.4 GeV 0-9\% Centrality}","P");

	sysFile->cd("RapidityDensity_PionMinus");
  //dNdyGraphHighTMulti->Add(onept,"AP");
  dNdyGraphHighTMulti->Write();
  dNdyGraphHighTMulti->SetMinimum(0.);
  dNdyGraphHighTMulti->SetMaximum(32.);
  dNdyGraphComparisonMulti->Write();
  dNdyGraphComparisonMulti->SetMinimum(0.);
  dNdyGraphComparisonMulti->SetMaximum(32.);
  leg->Write("legend");
  leg2->Write("leg2");
  starPrelim->Write("starPrelim");
  title->Write("title");
  //dNdyGraphHighTMulti->GetXaxis()->SetLabelSize(2);
  //dNdyGraphHighTMulti->GetYaxis()->SetLabelSize(2);

  for(Int_t iCentBin=0; iCentBin<nCentBins; ++iCentBin){
    cout << "Chi^2/NDF = " << gaus[iCentBin]->GetChisquare() <<"/"<< gaus[iCentBin]->GetNDF() <<endl;
    cout << "Amp = "<< gaus[iCentBin]->GetParameter(0) <<" +/- "<< gaus[iCentBin]->GetParError(0) <<endl;
    cout << "Mean = "<< gaus[iCentBin]->GetParameter(1) <<" +/- "<< gaus[iCentBin]->GetParError(1) <<endl;
    cout << "Sigma = "<< gaus[iCentBin]->GetParameter(2) <<" +/- "<< gaus[iCentBin]->GetParError(2) <<endl;
    gaus[iCentBin]->Write(Form("%s",gaus[iCentBin]->GetName()));
  }
  
}
